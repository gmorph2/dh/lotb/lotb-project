#!/usr/bin/env python

# Usage:
# 1. Run scalePath.py without any arguments. It'll be waiting on user input on STDIN.
# 2. Copy and paste the path part (the "d" attribute of the svg tag).
# 3. Press ENTER and Ctrl-D to end the input.
# 4. The output is printed to STDOUT after the line "OUTPUT:".

import fileinput
import re
import sys

from mako.template import Template

infile = sys.argv[1]

#scale = 2  # panel 2
scale = 20 / 3.0  # =~ 0.667 (panel 1)

chapter_color = '#c9f5c1'
scene_color = '#f1d0a9'

#color=chapter_color
color=scene_color

template='<svg xmlns="http://www.w3.org/2000/svg">${paths}</svg>'

path_template='<path xmlns="http://www.w3.org/2000/svg" d="${d}" data-paper-data="{&quot;rotation&quot;:0,&quot;annotation&quot;:null}" id="smooth_path_e0c94e07-0993-440f-bbd1-8154642129a8" fill-opacity="0" fill="%s" stroke="%s" stroke-width="23.50433" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="sans-serif" font-weight="normal" font-size="12" text-anchor="start" mix-blend-mode="normal"/>' % (color, color)

class App:
    def __init__(self, file_path):
        self.file_path = file_path

    def run(self):
        with open(self.file_path, 'r') as f:
            s = f.read()
            result = self.convert_content(s)
            print(result)

    def convert_content(self, s):
        groups = self.find_groups(s)
        if len(groups) > 1:
            print('WARNING: More than one group: %d. Working only on the 1st.'  % len(groups))
        return self.convert_group(groups[0])
            
    def find_groups(self, s):
        pattern = re.compile(r'(<g id="(.*?)".*?</g>)', re.MULTILINE | re.DOTALL)
        iter = re.finditer(pattern, s)
        groups = []
        for m in iter:
            id = m.group(2)
            if id != 'empty_outline':
              groups.append(m.group(1))
        return groups

    def convert_group(self, group):
        paths = self.find_paths(group)
        new_paths = []
        for path in paths:
            path_def = self.find_path_def(path)
            new_path = Template(path_template).render(d=path_def)
            new_paths.append(new_path)

        group = '<g transform="scale(%f)">%s</g>' % (scale, ''.join(new_paths))
        return Template(template).render(paths=group)

    def find_paths(self, s):
        pattern = re.compile(r'(<path.*?/>)', re.MULTILINE | re.DOTALL)
        iter = re.finditer(pattern, s)
        paths = []
        for m in iter:
            paths.append(m.group(1))
        return paths

    def find_path_def(self, s):
        m = re.search(r'd="(.*?)"', s, re.MULTILINE | re.DOTALL)
        return m.group(1)

if __name__ == '__main__':
    file_path = sys.argv[1]
    app = App(file_path)
    app.run()

# s = ''
# for line in fileinput.input():
#   s += line

# s = re.sub(r'[\d.]+', repl, s)
# s = re.sub(r'[ \r\n\t]+', '', s)  # remove whitespaces
# print('OUTPUT:')
# print(Template(template).render(d=s))
