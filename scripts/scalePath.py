#!/usr/bin/env python

# Usage:
# 1. Run scalePath.py without any arguments. It'll be waiting on user input on STDIN.
# 2. Copy and paste the path part (the "d" attribute of the svg tag).
# 3. Press ENTER and Ctrl-D to end the input.
# 4. The output is printed to STDOUT after the line "OUTPUT:".

import fileinput
import re
import sys

from mako.template import Template

#scale = 2  # panel 2
scale = 20 / 3.0  # =~ 0.667 (panel 1)

chapter_color = '#c9f5c1'
scene_color = '#f1d0a9'

#color=chapter_color
color=scene_color

template='<svg xmlns="http://www.w3.org/2000/svg"><path xmlns="http://www.w3.org/2000/svg" d="${d}" data-paper-data="{&quot;rotation&quot;:0,&quot;annotation&quot;:null}" id="smooth_path_e0c94e07-0993-440f-bbd1-8154642129a8" fill-opacity="0" fill="%s" stroke="%s" stroke-width="23.50433" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="sans-serif" font-weight="normal" font-size="12" text-anchor="start" mix-blend-mode="normal"/></svg>' % (color, color)

# String replacement callback
def repl(m):
    s = '%.1f' % (float(m.group()) * scale)
    return re.sub(r'\.0+$', '', s)

# Read string from STDIN.
print('Enter input string (an SVG path):')
s = ''
for line in fileinput.input():
  s += line

s = re.sub(r'[\d.]+', repl, s)
s = re.sub(r'[ \r\n\t]+', '', s)  # remove whitespaces
print('OUTPUT:')
print(Template(template).render(d=s))
